<?php

namespace App\Http\Controllers;

use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use Illuminate\Notifications\Notification;

class StoreController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $count = $user->notifications()->count();

        $stores = $user->stores()->simplePaginate();

        return view('stores/index', [
            'stores' => $stores,
            'user' => $user,
            'count' => $count,
        ]);
    }

    public function create()
    {
        $user = Auth::user();
        $count = $user->notifications()->count();

        return view('stores/create', [
            'count' => $count,
        ]);
    }

    public function store(CreateStoreRequest $request)
    {
        Auth::user()->stores()->create($request->validated());

        return to_route('stores.index');
    }

    public function edit(Store $store)
    {
        $user = Auth::user();
        $count = $user->notifications()->count();

        return view('stores/edit', [
            'store' => $store,
            'count' => $count,
        ]);
    }

    public function update(UpdateStoreRequest $request, Store $store)
    {
        $store->update($request->validated());

        return to_route('stores.index', [
            'store' => $store
        ]);
    }

    public function destroy(Store $store)
    {
        $store->delete();

        return to_route('stores.index');
    }
}
