<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = Auth::user()->notifications()->simplePaginate();

        // dd($notifications->data);

        return view("notifications.index", [
            "notifications" => $notifications,
        ]);
    }

    public function read($notification)
    {
        $notification = Auth::user()->notifications()->findOrFail($notification);

        $notification->markAsRead();

        return back();
    }

    public function destroy($notification)
    {
        $notification = Auth::user()->notifications()->findOrFail($notification);

        $notification->delete();

        return back();
    }


}
