<?php

namespace App\Notifications;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProductQuantity extends Notification
{
    use Queueable;

    private Product $product;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase($notifiable)
    {
        return [
            "tittle" => "Product quantity very low!",
            "massege" => sprintf(
                "Product: '%s' has %d pieces left",
                $this->product->name,
                $this->product->quantity,
            ),
            "links" => [
                'View Product' => route('products.view', [
                    'store' => $this->product->store->id,
                    'product' => $this->product->id,
                ], false),
            ],
        ];
    }
}
