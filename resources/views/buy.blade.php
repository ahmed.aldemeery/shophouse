<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <title>Buy Prouduct</title>
</head>
<body>
    <h1>Buy {{ $product->name }}</h1>
    <hr>
    @foreach($images as $image)
    <img src="{{ Storage::url($image->path) }}" alt="Product image" width="256" style="margin: 3px;">
    @endforeach
    <h2>Price : {{ $product->price }}$</h2>
    <h2>Ctegory : {{ $product->category->name }}</h2>
    @if($product->quantity <= 5 )
        <p style="color : orange">Only {{ $product->quantity }} left in store </p>
    @endif
    <h2>About : </h2>
    <p>{{ $product->about }}</p>

    <form action="{{ route('buynow', ['product' => $product->id]) }}" method="post">
        @csrf()

        <label for="quantity">Quantity</label>
        <input type="number" name="quanity" id="quantity" value="1">

        <label for="voucher">Voucher</label>
        <input type="text" name="vaucher" id="vaucher">

        <input type="submit" value="Buy now">

    </form>
</body>
</html>
