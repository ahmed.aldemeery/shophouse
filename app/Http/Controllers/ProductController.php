<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProducteRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Store;
use App\Models\Product;
use App\Models\Category;
use App\Notifications\ProductQuantity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    // SRP - Single Responsibility Principle

    public function index(Store $store)
    {
        $user = Auth::user();
        $count = $user->notifications()->count();

        $products = $store->products()->simplePaginate();

        // if ($product->quantity < 5) {
            // $user->notify(new ProductQuantity($product->name));
        // }

        return view("products/index", [
            "products" => $products,
            "store" => $store,
            "count" => $count,
        ]);
    }

    public function create(Store $store)
    {
        $user = Auth::user();
        $count = $user->notifications()->count();

        $categories = Category::get();

        return view('products.create', [
            "store" => $store,
            "categories" => $categories,
            "count" => $count,
        ]);
    }

    public function store(CreateProducteRequest $request, Store $store)
    {
        $product = $store->products()->create($request->validated());

        $images = $this->uploadImages($request->validated('images'));

        $product->images()->createMany($images);

        return to_route('products.index', ['store' => $store]);
    }


    public function edit(Store $store, Product $product)
    {
        $user = Auth::user();
        $count = $user->notifications()->count();

        $categories = Category::get();

        $images = $product->images()->get();

        return view('products.edit', [
            "product" => $product,
            "store" => $store,
            "categories" => $categories,
            "images" => $images,
            "count" => $count,
        ]);
    }

    public function update(UpdateProductRequest $request, Store $store, Product $product)
    {

        $product->update($request->validated());

        if ($request->has('images')) {

            $product->images()->delete();

            $images = $this->uploadImages($request->validated('images'));

            $product->images()->createMany($images);
        }

        return to_route('products.index', ["store" => $store, "product" => $product]);
    }

    public function destroy(Store $store, Product $product)
    {
        $product->delete();

        return to_route('products.index', ["product" => $product, "store" => $store]);
    }

    public function view(Store $store, Product $product)
    {
        $user = Auth::user();
        $count = $user->notifications()->unread()->count();

        $images = $product->images()->get();

        return view("products.show", [
            "store" => $store,
            "product" => $product,
            "images" => $images,
            "count" => $count,
        ]);
    }
    private function uploadImages(array $images): array
    {
        $data = [];

        foreach ($images as $image) {
            $temp = [];
            $temp['path'] = $image->store('public');
            $data[] = $temp;
        }

        return $data;
    }
}
