<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Product;
use Illuminate\Http\Request;

class BuyController extends Controller
{
    public function buy(Request $request, Product $product)
    {
        $images = $product->images()->simplePaginate();
        $store = Store::where('id', $product->store_id)->get();

        return view('buy', [
            "product" => $product,
            "images" => $images,
            "store" => $store
        ]);
    }

    public function buynow(Request $request, Product $product)
    {
        // 1. Validation:
        //        - Quantity must be less than or equal to the product quantity
        //        - Quantity must be a positive integer
        //        - If a voucher is used, ensure it exists in the database and the
        //          remaining is more than zero
        // 2. Calculate the total price
        // 3. If there is a voucher apply it and decrement the remaining of the voucher
        // 4. If the voucher remaining <= 5 then send a notification to the store owner
        // 5. Subtract quantity from product quantity
        // 6. If product quantity <= 5 then send a notification to the store owner
        // 7. Add the total price (after applying the voucher if one exists) to the
        //    wallet of the store owner
        // 8. If the wallet balance exceeds 1000, 5000, 50000, or 100000 send a notification
        //    to the store owner
        // 9. Take the user back to the home page

        dd('here');
    }
}
