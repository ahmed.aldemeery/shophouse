<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <title>Notifacations</title>
    <style>
        .delete-btn {padding: 0 !important;display: inline !important;background: transparent !important;color: var(--links) !important;}
        .delete-btn:hover {text-decoration: underline;}
        .delete-form {display: inline !important;vertical-align: middle !important;}
    </style>
</head>
<body>
    <h1>Notifacations</h1>
    <a href="{{ route('stores.index') }}">All Store</a> |
    <a href="{{ route('wallets.show', $wallet)  }}">Wallet</a> |
    <a href="{{ route('logout') }}">Logout</a>
    <hr>
    <table>
        @if ($notifications->isEmpty())
            <h1>No Notifications!</h1>
        @endif

        @foreach($notifications as $notification)
            <tr>
                <td>
                    @if($notification->unread())
                        <h3>{{ $notification->data['tittle'] }}</h3>
                    @else
                        <div>{{ $notification->data['tittle'] }}</div>
                    @endif

                    <p>{{ $notification->data['massege'] }}</p>

                    @if (isset($notification->data['links']))
                        @foreach ($notification->data['links'] as $title => $url)
                            <a href="{{ $url }}">{{ $title }}</a> |
                        @endforeach
                    @endif
                    @if($notification->unread())
                        <a href="{{ route("notifications.read", $notification) }}">Mark as read</a> |
                    @endif
                    <form onsubmit="return confirm('Are you sure?');" class="delete-form" action="{{route('notifications.destroy', $notification) }}" method="POST">
                        @method('DELETE')
                        @csrf()
                        <input class="delete-btn" type="submit" value="Delete">
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {{ $notifications->links() }}
</body>
</html>
