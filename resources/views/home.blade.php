<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
    <title>Home</title>
</head>
<body>
    <h1>Home</h1>
    <hr>
    <hr>
    @foreach ($products as $product)

        <div>Name : {{ $product -> name }}</div>
        <div>Price : {{ $product -> price }}$</div>

        @foreach ($product->images as $image)
        <img src="{{ Storage::url($image->path) }}" alt="Product image" width="256" style="margin: 3px;">
        @endforeach
        <br>
        <a href="{{ route('buy', ['product' => $product->id]) }}">Buy</a>
        <hr>
    @endforeach

</body>
</html>
