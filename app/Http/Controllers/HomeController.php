<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(Request $request)
    {
        return view('home', [
            'products' => Product::with('images', 'reviews', 'store', 'category')->simplePaginate(),
        ]);

    }
}
